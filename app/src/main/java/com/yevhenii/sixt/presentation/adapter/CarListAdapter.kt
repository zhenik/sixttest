package com.yevhenii.sixt.presentation.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.yevhenii.sixt.R
import com.yevhenii.sixt.presentation.entity.CarViewEntity

class CarListAdapter(private var carList: ArrayList<CarViewEntity>, private val context: Context?) :
    RecyclerView.Adapter<CarViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CarViewHolder(
        LayoutInflater.from(context).inflate(
            R.layout.item_list_car,
            parent,
            false
        )
    )

    override fun getItemCount() = carList.size


    override fun onBindViewHolder(holder: CarViewHolder, position: Int) {
        val carItem = carList[position]
        holder.bind(carItem)
    }

}


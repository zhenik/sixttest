package com.yevhenii.sixt.presentation.fragment

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v4.app.Fragment
import com.yevhenii.sixt.App
import com.yevhenii.sixt.presentation.entity.CarViewEntity
import com.yevhenii.sixt.presentation.viewmodel.CarListViewModel
import javax.inject.Inject

abstract class BaseCarFragment : Fragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    protected lateinit var vm: CarListViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        App.component.inject(this)
        vm = ViewModelProviders.of(this, viewModelFactory)[CarListViewModel::class.java]
        vm.dataList.observe(this, Observer { updateCarList(it) })
    }

    abstract fun updateCarList(list: List<CarViewEntity>?)
}
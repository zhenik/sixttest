package com.yevhenii.sixt.presentation.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.yevhenii.sixt.R
import com.yevhenii.sixt.domain.model.Car
import com.yevhenii.sixt.presentation.adapter.CarListAdapter
import com.yevhenii.sixt.presentation.entity.CarViewEntity
import kotlinx.android.synthetic.main.fragment_car_list.*
import android.support.v7.widget.DividerItemDecoration
import android.widget.LinearLayout.VERTICAL


class CarListFragment : BaseCarFragment() {

    lateinit var adapter: CarListAdapter
    var list: ArrayList<CarViewEntity> = ArrayList()

    companion object {
        fun newInstance(): Fragment {
            return CarListFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_car_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm.get()

        initUi()
    }

    private fun initUi() {
        adapter = CarListAdapter(list, context)
        val dividerItemDecoration = DividerItemDecoration(context, VERTICAL)

        rvCarList.addItemDecoration(dividerItemDecoration)
        rvCarList.adapter = adapter
        rvCarList.layoutManager = LinearLayoutManager(context)

    }

    override fun updateCarList(list: List<CarViewEntity>?) {
        list?.let {
            this.list.clear()
            this.list.addAll(ArrayList(it))
            adapter.notifyDataSetChanged()
        }
    }
}
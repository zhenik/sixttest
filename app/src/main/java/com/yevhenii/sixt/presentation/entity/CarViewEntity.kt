package com.yevhenii.sixt.presentation.entity

import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterItem
import com.yevhenii.sixt.domain.model.Car
import kotlin.math.roundToInt
import com.yevhenii.sixt.presentation.utils.PicassoMarker


class CarViewEntity constructor(
    val id: String,
    val modelIdentifier: String,
    val modelName: String,
    val name: String,
    val make: String,
    val group: String,
    val color: String,
    val series: String,
    val fuelType: String,
    val fuelLevel: String,
    val transmission: String,
    val licensePlate: String,
    val latLng: LatLng,
    val innerCleanliness: String,
    val carImageUrl: String?
) : ClusterItem {

    var picassoMarker: PicassoMarker? = null

    override fun getSnippet(): String {
        return "Id: ${this.id}"

    }

    override fun getTitle(): String {
        return this.modelName
    }

    override fun getPosition(): LatLng {
        return this.latLng
    }
}


fun Car.mapToViewEntity(): CarViewEntity {

    val innerCleanliness =
        when (this.innerCleanliness) {
            "VERY_CLEAN" -> "Very Clean"
            "CLEAN" -> "Clean"
            "REGULAR" -> "Regular"
            else -> ""
        }
    val fuelLevel = "${(this.fuelLevel * 100).roundToInt()}%"

    return CarViewEntity(
        this.id,
        this.modelIdentifier,
        this.modelName,
        this.name,
        this.make,
        this.group,
        this.color,
        this.series,
        this.fuelType,
        fuelLevel,
        this.transmission,
        this.licensePlate,
        LatLng(this.latitude, this.longitude),
        innerCleanliness,
        this.carImageUrl
    )
}

fun List<Car>.mapToListViewEntity(): List<CarViewEntity> {
    val list = ArrayList<CarViewEntity>()
    for (car in this) {
        list.add(car.mapToViewEntity())
    }
    return list
}

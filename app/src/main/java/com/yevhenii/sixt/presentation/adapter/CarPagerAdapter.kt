package com.yevhenii.sixt.presentation.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.yevhenii.sixt.presentation.fragment.CarListFragment
import com.yevhenii.sixt.presentation.fragment.CarMapFragment

class CarPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
    override fun getItem(position: Int): Fragment? {
        when (position) {
            0 -> return CarListFragment.newInstance()
            1 -> return CarMapFragment.newInstance()
        }
        return null
    }

    override fun getCount(): Int {
        return 2
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
            0 -> "List"
            1 -> "Map"
            else -> null
        }
    }
}
package com.yevhenii.sixt.presentation.activity

import android.os.Bundle
import com.yevhenii.sixt.R
import com.yevhenii.sixt.presentation.adapter.CarPagerAdapter
import com.yevhenii.sixt.presentation.entity.CarViewEntity
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val adapter = CarPagerAdapter(supportFragmentManager)

        vpCars.adapter = adapter
        tabLayout.setupWithViewPager(vpCars)
    }


}

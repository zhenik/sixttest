package com.yevhenii.sixt.presentation.adapter

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.squareup.picasso.Picasso
import com.yevhenii.sixt.R
import com.yevhenii.sixt.presentation.entity.CarViewEntity

class CarViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    var ivCar: ImageView = itemView.findViewById(R.id.ivCar)
    var tvCarName: TextView = itemView.findViewById(R.id.tvCarName)
    var tvInnerCleanliness: TextView = itemView.findViewById(R.id.tvInnerCleanliness)
    var tvTransmission: TextView = itemView.findViewById(R.id.tvTransmission)
    var tvFuelType: TextView = itemView.findViewById(R.id.tvFuelType)
    var tvFuelLevel: TextView = itemView.findViewById(R.id.tvFuelLevel)
    var tvLicencePlate: TextView = itemView.findViewById(R.id.tvLicencePlate)
    var tvColor: TextView = itemView.findViewById(R.id.tvColor)
    var tvSeries: TextView = itemView.findViewById(R.id.tvSeries)
    var tvName: TextView = itemView.findViewById(R.id.tvName)
    var tvId: TextView = itemView.findViewById(R.id.tvId)

    fun bind(carItem: CarViewEntity) {
        tvCarName.text = carItem.modelName
        tvTransmission.text = carItem.transmission
        tvInnerCleanliness.text = carItem.innerCleanliness
        tvFuelLevel.text = carItem.fuelLevel
        tvFuelType.text = carItem.fuelType
        tvLicencePlate.text = carItem.licensePlate
        tvColor.text = carItem.color
        tvSeries.text = carItem.series
        tvName.text = carItem.name
        tvId.text = carItem.id
        Picasso.get().load(carItem.carImageUrl).placeholder(R.drawable.car_empty).into(ivCar)
    }

}
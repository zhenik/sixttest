package com.yevhenii.sixt.presentation.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.ImageView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.clustering.Cluster
import com.google.maps.android.clustering.ClusterManager
import com.google.maps.android.clustering.view.DefaultClusterRenderer
import com.google.maps.android.ui.IconGenerator
import com.squareup.picasso.Picasso
import com.yevhenii.sixt.R
import com.yevhenii.sixt.presentation.entity.CarViewEntity


class CarClusterRenderer(
    var context: Context?, var layoutInflater: LayoutInflater?,
    map: GoogleMap?,
    clusterManager: ClusterManager<CarViewEntity>?
) : DefaultClusterRenderer<CarViewEntity>(context, map, clusterManager) {

    private val markerCarItemMap = HashMap<CarViewEntity, Marker>()

    private val iconGenerator = IconGenerator(context)
    private val iconClusterGenerator = IconGenerator(context)

    init {
        val view: View? = layoutInflater?.inflate(R.layout.cluster_item, null)
        iconClusterGenerator.setContentView(view)
        iconClusterGenerator.setBackground(null)
    }

    override fun onBeforeClusterRendered(cluster: Cluster<CarViewEntity>?, markerOptions: MarkerOptions?) {
        super.onBeforeClusterRendered(cluster, markerOptions)
        cluster?.let {
            val bikeCount = Integer.toString(it.size)
            val icon = iconClusterGenerator.makeIcon(bikeCount)
            markerOptions?.icon(BitmapDescriptorFactory.fromBitmap(icon))
            for (bikeClusterItem in it.items) {
                markerCarItemMap.remove(bikeClusterItem)
            }
        }

    }

    override fun onBeforeClusterItemRendered(bikeClusterItem: CarViewEntity, markerOptions: MarkerOptions) {
        val icon = iconGenerator.makeIcon()
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon))
    }

    override fun onClusterItemRendered(clusterItem: CarViewEntity?, marker: Marker?) {
        super.onClusterItemRendered(clusterItem, marker)
        clusterItem?.let {
            marker?.let { markr ->
                markerCarItemMap[it] = markr
                loadIconFromApi(it, markr)
            }
        }
    }

    private fun loadIconFromApi(bikeClusterItem: CarViewEntity, marker: Marker) {

        val markerView: View? = layoutInflater?.inflate(R.layout.marker_item, null)
        val iv = markerView?.findViewById(R.id.ivCar) as ImageView
        iconGenerator.setContentView(markerView)
        iconGenerator.setBackground(null)

        val picassoMarker = PicassoMarker(
            marker,
            iconGenerator,
            iv
        )
        Picasso.get()
            .load(bikeClusterItem.carImageUrl)
            .placeholder(R.drawable.car_empty)
            .into(picassoMarker)

        bikeClusterItem.picassoMarker = picassoMarker
    }
}
package com.yevhenii.sixt.presentation.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.common.GooglePlayServicesNotAvailableException
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.maps.android.clustering.ClusterManager
import com.yevhenii.sixt.presentation.entity.CarViewEntity

import com.yevhenii.sixt.R
import com.yevhenii.sixt.presentation.utils.CarClusterRenderer


class CarMapFragment : BaseCarFragment(), OnMapReadyCallback {

    var map: GoogleMap? = null
    var mapView: MapView? = null

    private var mClusterManager: ClusterManager<CarViewEntity>? = null

    companion object {
        fun newInstance(): Fragment {
            return CarMapFragment()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val v = inflater.inflate(R.layout.fragment_car_map, container, false);

        mapView = v.findViewById(R.id.mapView) as MapView
        mapView?.onCreate(savedInstanceState)
        mapView?.getMapAsync(this)

        try {
            MapsInitializer.initialize(this.activity)
        } catch (e: GooglePlayServicesNotAvailableException) {
            e.printStackTrace()
        }


        return v
    }

    override fun updateCarList(list: List<CarViewEntity>?) {


        list?.let {
            mClusterManager?.addItems(it)
            val renderer = CarClusterRenderer(
                activity, activity?.layoutInflater, map, mClusterManager
            )
            mClusterManager?.renderer = renderer

            val cameraUpdate = CameraUpdateFactory.newLatLngZoom(calculateAverageLatLng(it), 10f)
            map?.animateCamera(cameraUpdate)


        }
    }

    private fun calculateAverageLatLng(list: List<CarViewEntity>): LatLng {
        var latAvg = 0.0
        var lngAvg = 0.0
        for (item in list) {
            latAvg += item.latLng.latitude
            lngAvg += item.latLng.longitude
        }
        latAvg /= list.size
        lngAvg /= list.size
        return LatLng(latAvg, lngAvg)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map = googleMap
        map?.uiSettings?.isZoomControlsEnabled = true
        mClusterManager = ClusterManager(context, map)
        map?.setOnCameraIdleListener(mClusterManager)
        vm.get()

    }

    override fun onResume() {
        mapView?.onResume()
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        mapView?.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView?.onLowMemory()
    }


}
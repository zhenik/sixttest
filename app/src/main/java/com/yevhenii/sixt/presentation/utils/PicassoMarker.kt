package com.yevhenii.sixt.presentation.utils

import android.graphics.drawable.Drawable
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import android.graphics.Bitmap
import android.widget.ImageView
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import com.google.android.gms.maps.model.Marker
import com.google.maps.android.ui.IconGenerator
import java.lang.Exception

class PicassoMarker internal constructor(
    var mMarker: Marker, private var mIconGenerator: IconGenerator, private var mImgBike: ImageView
) : Target {


    override fun hashCode(): Int {
        return mMarker.hashCode()
    }

    override fun equals(other: Any?): Boolean {
        return if (other is PicassoMarker) {
            val marker = other.mMarker
            mMarker == marker
        } else {
            false
        }
    }

    override fun onBitmapLoaded(bitmap: Bitmap, from: Picasso.LoadedFrom) {
        mImgBike.setImageBitmap(bitmap)
        val icon = mIconGenerator.makeIcon()
        try {
            mMarker.setIcon(BitmapDescriptorFactory.fromBitmap(icon))
        } catch (exception: IllegalArgumentException) {
        }

    }

    override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
    }

    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {

    }
}
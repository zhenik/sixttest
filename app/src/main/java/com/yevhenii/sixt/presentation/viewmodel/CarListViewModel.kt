package com.yevhenii.sixt.presentation.viewmodel

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.yevhenii.sixt.App
import com.yevhenii.sixt.di.components.AppComponent
import com.yevhenii.sixt.domain.model.Car
import com.yevhenii.sixt.domain.usecase.GetCars
import com.yevhenii.sixt.presentation.entity.CarViewEntity
import com.yevhenii.sixt.presentation.entity.mapToListViewEntity
import io.reactivex.observers.DisposableObserver
import javax.inject.Inject

class CarListViewModel @Inject constructor(private val getCarsUseCase: GetCars) : ViewModel() {

    val dataList: MutableLiveData<List<CarViewEntity>> = MutableLiveData()

    init {
        val appComponent: AppComponent = App.component
        appComponent.inject(this)
    }

    fun get() =
        getCarsUseCase.execute(object : DisposableObserver<List<Car>>() {
            override fun onComplete() {
            }
            override fun onNext(t: List<Car>) {
                dataList.postValue(t.mapToListViewEntity())
            }
            override fun onError(e: Throwable) {
            }
        })


}
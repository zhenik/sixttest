package com.yevhenii.sixt

import android.app.Application
import com.yevhenii.sixt.di.components.AppComponent
import com.yevhenii.sixt.di.components.DaggerAppComponent
import com.yevhenii.sixt.di.modules.ApiModule
import com.yevhenii.sixt.di.modules.ContextModule

class App : Application() {
    companion object {
        lateinit var component: AppComponent
            private set
    }

    override fun onCreate() {
        super.onCreate()
        component = DaggerAppComponent.builder()
            .contextModule(ContextModule(this))
            .apiModule(ApiModule("https://cdn.sixt.io/"))
            .build()
    }
}
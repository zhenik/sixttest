package com.yevhenii.sixt.di.modules

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.yevhenii.sixt.presentation.viewmodel.CarListViewModel
import com.yevhenii.sixt.presentation.viewmodel.ViewModelFactory
import com.yevhenii.sixt.presentation.viewmodel.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(CarListViewModel::class)
    internal abstract fun postListViewModel(viewModel: CarListViewModel): ViewModel

}
package com.yevhenii.sixt.di.modules

import com.yevhenii.sixt.data.api.CarsApi
import com.yevhenii.sixt.data.repository.CarsRepository
import com.yevhenii.sixt.domain.Repository
import com.yevhenii.sixt.domain.SchedulersProvider
import com.yevhenii.sixt.domain.usecase.GetCars
import com.yevhenii.sixt.presentation.AndroidSchedulersProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MainModule {
    @Provides
    @Singleton
    fun provideWithSchedulers(): SchedulersProvider =
        AndroidSchedulersProvider()

    @Provides
    @Singleton
    fun repository(carsApi: CarsApi): Repository = CarsRepository(carsApi)

    @Provides
    @Singleton
    fun getCarsUseCase(
        carsRepository: Repository,
        schedulersProvider: SchedulersProvider
    ): GetCars = GetCars(carsRepository, schedulersProvider)
}
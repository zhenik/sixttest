package com.yevhenii.sixt.di.components

import android.content.Context
import android.support.v7.widget.RecyclerView
import com.yevhenii.sixt.di.modules.*
import com.yevhenii.sixt.presentation.fragment.BaseCarFragment
import com.yevhenii.sixt.presentation.viewmodel.CarListViewModel
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [ApiModule::class, ContextModule::class, OkHttpClientModule::class, MainModule::class, ViewModelModule::class])
interface AppComponent {
    fun context(): Context

    fun inject(presenter: CarListViewModel)
    fun inject(presenter: BaseCarFragment)

    fun inject(viewHolder: RecyclerView.ViewHolder)
}
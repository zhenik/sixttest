package com.yevhenii.sixt.domain

import com.yevhenii.sixt.domain.model.Car
import io.reactivex.Observable


interface Repository {
    fun getCarList(): Observable<List<Car>>
}
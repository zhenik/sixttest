package com.yevhenii.sixt.domain.usecase

import com.yevhenii.sixt.domain.SchedulersProvider
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver


abstract class UseCase<T>(private val schedulersProvider: SchedulersProvider) {

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun execute(disposableObserver: DisposableObserver<T>?) {

        if (disposableObserver == null) {
            throw IllegalArgumentException("disposableObserver must not be null")
        }

        val observable =
            this.createObservableUseCase().subscribeOn(schedulersProvider.backgroundThread())
                .observeOn(schedulersProvider.uiThread())

        val observer = observable.subscribeWith(disposableObserver)
        compositeDisposable.add(observer)
    }

    fun dispose() {
        if (!compositeDisposable.isDisposed) {
            compositeDisposable.dispose()
        }
    }

    protected abstract fun createObservableUseCase(): Observable<T>
}

package com.yevhenii.sixt.domain

import io.reactivex.Scheduler

interface SchedulersProvider {
    fun uiThread(): Scheduler
    fun backgroundThread(): Scheduler
}
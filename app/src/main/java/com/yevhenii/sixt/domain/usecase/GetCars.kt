package com.yevhenii.sixt.domain.usecase

import com.yevhenii.sixt.domain.Repository
import com.yevhenii.sixt.domain.SchedulersProvider
import com.yevhenii.sixt.domain.model.Car
import io.reactivex.Observable

class GetCars constructor(
    private var carRepository: Repository,
    private var schedulersProvider: SchedulersProvider
) : UseCase<List<Car>>(schedulersProvider) {

    override fun createObservableUseCase(): Observable<List<Car>> {
        return carRepository.getCarList()
            .subscribeOn(schedulersProvider.backgroundThread())
            .observeOn(schedulersProvider.uiThread())
    }
}
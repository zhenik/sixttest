package com.yevhenii.sixt.data.entity

import com.google.gson.annotations.SerializedName
import com.yevhenii.sixt.domain.model.Car

data class CarEntity(
    @SerializedName("id") val id: String,
    @SerializedName("modelIdentifier") val modelIdentifier: String,
    @SerializedName("modelName") val modelName: String,
    @SerializedName("name") val name: String,
    @SerializedName("make") val make: String,
    @SerializedName("group") val group: String,
    @SerializedName("color") val color: String,
    @SerializedName("series") val series: String,
    @SerializedName("fuelType") val fuelType: String,
    @SerializedName("fuelLevel") val fuelLevel: Float,
    @SerializedName("transmission") val transmission: String,
    @SerializedName("licensePlate") val licensePlate: String,
    @SerializedName("latitude") val latitude: Double,
    @SerializedName("longitude") val longitude: Double,
    @SerializedName("innerCleanliness") val innerCleanliness: String,
    @SerializedName("carImageUrl") val carImageUrl: String
)

fun CarEntity.toCar(): Car = Car(
    this.id,
    this.modelIdentifier,
    this.modelName,
    this.name,
    this.make,
    this.group,
    this.color,
    this.series,
    this.fuelType,
    this.fuelLevel,
    this.transmission,
    this.licensePlate,
    this.latitude,
    this.longitude,
    this.innerCleanliness,
    this.carImageUrl
)
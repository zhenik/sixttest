package com.yevhenii.sixt.data.api

import com.yevhenii.sixt.data.entity.CarEntity
import io.reactivex.Observable
import retrofit2.http.GET

interface CarsApi {
    @GET("codingtask/cars")
    fun getCarList(): Observable<List<CarEntity>>
}
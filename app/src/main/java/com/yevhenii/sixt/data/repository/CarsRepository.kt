package com.yevhenii.sixt.data.repository

import com.yevhenii.sixt.data.api.CarsApi
import com.yevhenii.sixt.data.entity.toCar
import com.yevhenii.sixt.domain.Repository
import com.yevhenii.sixt.domain.model.Car
import io.reactivex.Observable

class CarsRepository(private val carsApi: CarsApi) : Repository {
    override fun getCarList(): Observable<List<Car>> {
        return carsApi.getCarList()
            .flatMapIterable { it }
            .map { it.toCar() }
            .toList()
            .toObservable()
    }
}